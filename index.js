//2.
db.fruits.aggregate([
   { $match: { onSale: true } }, 
   { $count: "fruitsOnSale" }
   
   ]).pretty();


//3.
db.fruits.aggregate([
   { $match: { stock: {$gte:20} } }, 
   { $count: "enoughStock" }
   
   ]).pretty();


//4.
db.fruits.aggregate([
   {$match : {onSale:true}},
   {$group: {_id:"$supplier_id", avg_price : {$avg: "$price"}}},
   ]).pretty();


//5.
db.fruits.aggregate([
   {$match : {onSale:true}},
   { $group : { _id: "$supplier_id", max_price: { $max : "$price" }}},
   ]).pretty();


//6.
db.fruits.aggregate([
   {$match : {onSale:true}},
   { $group : { _id: "$supplier_id", min_price: { $min : "$price" }}},
   ]).pretty();
